package com.example.githubclient.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.githubclient.databinding.RepoListItemBinding
import com.example.githubclient.ui.RepoClickListener
import com.example.githubclient.ui.models.Repository

class ReposAdapter(
    private val itemClickListener: RepoClickListener,
    private val repos: MutableList<Repository> = mutableListOf()
) : RecyclerView.Adapter<ReposAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = RepoListItemBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(repos[position])
    }

    override fun getItemCount() = repos.size

    fun update(newRepos: List<Repository>) {
        repos.clear()
        repos.addAll(newRepos)
        notifyItemRangeInserted(0, repos.size)
    }

    inner class ViewHolder(private val binding: RepoListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(repo: Repository) = with(binding) {
            repoName.text = repo.name
            repoDescription.text = repo.description
            root.setOnClickListener {
                itemClickListener.onClick(repo)
            }
        }
    }


}