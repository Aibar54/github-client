package com.example.githubclient.ui

import com.example.githubclient.ui.models.Repository

interface RepoClickListener {
    fun onClick(repo: Repository)
}