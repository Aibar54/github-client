package com.example.githubclient.ui.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.githubclient.R
import com.example.githubclient.databinding.HomeScreenFragmentBinding
import com.example.githubclient.ui.MainViewModel
import com.example.githubclient.ui.RepoClickListener
import com.example.githubclient.ui.adapter.ReposAdapter
import com.example.githubclient.ui.models.Repository
import com.google.android.material.snackbar.Snackbar

class HomeScreenFragment : Fragment(), RepoClickListener {

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: HomeScreenFragmentBinding
    private lateinit var adapter: ReposAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = HomeScreenFragmentBinding.inflate(inflater, container, false)
        setUpView()
        return binding.root
    }

    override fun onClick(repo: Repository) {
        val fragment = RepoDetailFragment(repo)
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .addToBackStack(null)
            .commit()
    }

    private fun setUpView() {
        adapter = ReposAdapter(this as RepoClickListener)
        binding.list.adapter = adapter

        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
        setUpViewModel()

        binding.button.setOnClickListener {
            hideKeyboard()
            val organization = binding.input.text.toString()
            viewModel.getReps(organization)
        }
    }

    private fun setUpViewModel() {
        viewModel.repos.observe(requireActivity()) {
            adapter.update(it)
        }
        viewModel.isLoading.observe(requireActivity()) {
            binding.progressBar.visibility = if (it) View.VISIBLE else View.GONE
        }
        viewModel.error.observe(requireActivity()) {
            if (it) showErrorMessage()
        }
    }

    private fun hideKeyboard() {
        val imm = requireView().context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(requireView().windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }

    private fun showErrorMessage() {
        val message = "Error"
        Snackbar.make(requireActivity(), requireView(), message, Snackbar.LENGTH_SHORT).show()
    }
}