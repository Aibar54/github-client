package com.example.githubclient.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.githubclient.api.RetrofitApiFactory
import com.example.githubclient.ui.models.Repository
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private val _repos: MutableLiveData<List<Repository>> = MutableLiveData()
    val repos: LiveData<List<Repository>>
        get() = _repos

    private val _isLoading = MutableLiveData(false)
    val isLoading: LiveData<Boolean>
        get() = _isLoading

    private val _error = MutableLiveData(false)
    val error: LiveData<Boolean>
        get() = _error

    fun getReps(organization: String) = viewModelScope.launch {
        try {
            _isLoading.value = true
            _error.value = false
            val api = RetrofitApiFactory.createInstance(organization)
            val fetchedRepos = api.getReposShortInfo()
            _repos.value = fetchedRepos
        } catch (e: Exception) {
            _error.value = true
        } finally {
            _isLoading.value = false
        }
    }
}