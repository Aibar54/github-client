package com.example.githubclient.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.githubclient.R
import com.example.githubclient.databinding.RepoDetailFragmentBinding
import com.example.githubclient.ui.models.Repository

class RepoDetailFragment(private val repo: Repository) : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = RepoDetailFragmentBinding.inflate(inflater, container, false)
        with(binding) {
            repoName.text = repo.name
            forks.text = getString(R.string.forks, repo.forks)
            watchers.text = getString(R.string.watchers, repo.watchers)
            issues.text = getString(R.string.issues, repo.issues)
            repoDescription.text = getString(R.string.description, repo.description)
        }
        return binding.root
    }
}