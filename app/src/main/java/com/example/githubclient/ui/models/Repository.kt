package com.example.githubclient.ui.models

data class Repository(
    val id: Int,
    val name: String,
    val description: String,
    val forks: Int,
    val watchers: Int,
    val issues: Int,
    val parent: Parent
)