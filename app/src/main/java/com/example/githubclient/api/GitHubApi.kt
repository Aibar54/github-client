package com.example.githubclient.api

import com.example.githubclient.ui.models.Repository
import retrofit2.http.GET

interface GitHubApi {

    @GET("repos")
    suspend fun getReposShortInfo(): List<Repository>
}