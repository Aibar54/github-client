package com.example.githubclient.api

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

private const val BASE_URL = "https://api.github.com/orgs/"
object RetrofitApiFactory {

    fun createInstance(organization: String): GitHubApi {
        val retrofit = Retrofit.Builder()
                .baseUrl("$BASE_URL$organization/")
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
        return retrofit.create(GitHubApi::class.java)
    }
}